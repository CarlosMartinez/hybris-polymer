/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.ow.pocs.core.constants;

/**
 * Global class for all OwpocsCore constants. You can add global constants for your extension into this class.
 */
public final class OwpocsCoreConstants extends GeneratedOwpocsCoreConstants
{
	public static final String EXTENSIONNAME = "owpocscore";


	private OwpocsCoreConstants()
	{
		//empty
	}

	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS ="quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS ="quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
}
