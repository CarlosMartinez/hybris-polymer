
export default class OwTest {
  doSomething() {
    console.log('this is another test!')
  }
}

$(document).ready(() => {
  const test = new OwTest()
  test.doSomething()
  console.log(OwTest.doSomething)
})

var OW = OW || {}

OW.test = {

  test: function () {
    console.log('this is a test')
  }

}
