ACC.minicart = {

	_autoload: [
		"bindMiniCart"
	],

	bindMiniCart: function(){

        $(document).on("click",".js-mini-cart-link", function(e){
            e.preventDefault();
            var url = $(this).data("miniCartUrl");

            $.ajax({
                url: url
            }).done(function (data) {
                var miniCartContainer = $("#miniCartModal");
                miniCartContainer.find(".mini-cart-modal-container").html("").append(data);
                miniCartContainer.removeClass("cboxElement").modal("show");
            });




        });

        $(document).on("click",".js-mini-cart-close-button", function(e){
            e.preventDefault();
            ACC.colorbox.close();
        });
    },

    updateMiniCartDisplay: function(){
        var cartItems = $(".js-mini-cart-link").data("miniCartItemsText");
        var miniCartRefreshUrl = $(".js-mini-cart-link").data("miniCartRefreshUrl");
        $.ajax({
            url: miniCartRefreshUrl,
            cache: false,
            type: 'GET',
            success: function(jsonData){
                $(".js-mini-cart-link .js-mini-cart-count").html('<span class="nav-items-total">' + jsonData.miniCartCount + '<span class="items-desktop hidden-xs hidden-sm">' + ' ' + cartItems + '</span>' + '</span>' );
                $(".js-mini-cart-link .js-mini-cart-price").html(jsonData.miniCartPrice);
            }
        });
    }

};