<div id="miniCartModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cart</h4>
            </div>
            <div class="modal-body">
                <div class="mini-cart-modal-container"></div>
            </div>
        </div>
    </div>
</div>