package com.ow.pocs.storefront.controllers.rest;

import com.ow.pocs.storefront.dtos.UpdateCartDto;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.UpdateQuantityForm;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * Created by melissaaracelyaguilargonzalez on 07/05/17.
 */
@RestController
@RequestMapping("/rest/cart")
public class CartRestController extends AbstractCartPageController
{
	@Resource(name="cartFacade")
	private CartFacade cartFacade;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String testt()
	{
		return "hmmmm..";
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public UpdateCartDto updateCartQuantities(@RequestParam("entryNumber") final long entryNumber,
			@Valid final UpdateQuantityForm form, final BindingResult bindingResult) throws CMSItemNotFoundException
	{
		UpdateCartDto response = new UpdateCartDto();
		response.setSuccess(false);
		if (bindingResult.hasErrors())
		{
			for (final ObjectError error : bindingResult.getAllErrors())
			{
				if ("typeMismatch".equals(error.getCode()))
				{
					response.setErrorMessage("type mismatch :V");
				}
				else
				{
					response.setErrorMessage(error.getDefaultMessage());
				}
			}
		}
		else if (getCartFacade().hasEntries())
		{
			try
			{
				final CartModificationData cartModification = getCartFacade().updateCartEntry(entryNumber,
						form.getQuantity().longValue());
				response.setUpdate(cartModification);
				response.setSuccess(true);
				response.setNewCart(cartFacade.getSessionCart());
			}
			catch (final CommerceCartModificationException ex)
			{
				response.setErrorMessage("Something failed :V");
				LOG.warn("Couldn't update product with the entry number: " + entryNumber + ".", ex);
			}
		}

		return response;
	}
}
