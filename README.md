
# About

This project is meant to be an example about how to integrate [Google Polymer](https://www.polymer-project.org/) into a hybris accelerator module (B2C) and how to develop using it properly


![Polymer](https://www.polymer-project.org/images/logos/p-logo.png "Polymer Project")
![hybris](http://cdn.blog.cpcstrategy.com/wp-content/uploads/2014/01/hybris-review-logo.jpg "SAP hybris")


**Note**: Just in case you wonder *owpocs* stands for ObjectWave PoCs

## Configuration

### hybris

The basic configuration for the hybris project to work is located under local_config/ directory

### Polymer

For Polymer to work you need the following dependencies installed on your local environment:

* bower: The old school storefront-related package manager

### Setup

You already know how to setup hybris on your local environment so we wil focus on Polymer here..

1. Install the bower dependencies by running

```
#!bash
# go to the folder
cd owpocsstorefront/web/webroot/_ui/responsive/common/js/components/polymer/
# install the dependencies
bower install
```

That's it - at least for now :V-


## Nice to have

* Write a build callback to the main hybris task to install the dependencies
* If we still have energy to keep thinking.. Integrate mocka **<3** to add unit tests for each component
* Maybe integrate the [Polymer CLI](https://www.polymer-project.org/1.0/docs/tools/polymer-cli) tool to run the build tasks integrated to that
* If we can't integrate the CLI, find a way to integrate [eslint](http://eslint.org/) for code quality check
* If we can't integrate the CLI, find a way to integrate any build tool to minify scripts with lazy load support, for example: browserify, webpack


# Authors :)

* Melissa Aracely Aguilar González - Super software developer with unicorn-like powers
* Carlos José Martínez Arenas - The guy who's always complaining about hybris


TODO: add the documentation of the final work
